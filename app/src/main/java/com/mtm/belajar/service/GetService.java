package com.mtm.belajar.service;

import com.mtm.belajar.model.DataModel;
import com.mtm.belajar.model.RetroPhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetService {
    @GET("/photos")
    Call<List<RetroPhoto>> getAllPhotos();
    @GET("/api/v1/products.json?brand=maybelline")
    Call<List<DataModel>> getListMakeUp();
}
