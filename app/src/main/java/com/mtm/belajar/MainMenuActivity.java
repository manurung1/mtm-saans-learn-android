package com.mtm.belajar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenuActivity extends AppCompatActivity {
    //    private LinearLayout id_goto_main;
    private TextView id_nama_button;
    private LinearLayout id_goto_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        iniComponent();




    }

    public  void iniComponent(){
        id_nama_button = findViewById(R.id.id_nama_button);
        id_nama_button.setText("BUTTON LANJUT");
        id_goto_main = findViewById(R.id.id_goto_main);
        id_goto_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainMenuActivity.this, "BRO MAU KE LAYOUT 2 DONG", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainMenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

}