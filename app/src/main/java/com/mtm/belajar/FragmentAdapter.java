package com.mtm.belajar;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtm.belajar.model.DataModel;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FragmentAdapter extends RecyclerView.Adapter<FragmentAdapter.FragmentViewHolder> {
    private List<DataModel> DataModel;
    private Context context;

    public FragmentAdapter(Context context, List<DataModel> DataModel) {
        this.context = context;
        this.DataModel = DataModel;
    }


    class FragmentViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        private ImageView coverImage;
        private TextView id_name, id_price, id_rating, id_detail_produk;
        private LinearLayout id_click_produk;
        private Button id_visit_web;

        FragmentViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            coverImage = mView.findViewById(R.id.coverImage);
            id_name = mView.findViewById(R.id.id_name);
            id_price = mView.findViewById(R.id.id_price);
            id_rating = mView.findViewById(R.id.id_rating);
            id_click_produk = mView.findViewById(R.id.id_click_produk);
            id_visit_web = mView.findViewById(R.id.id_visit_web);
            id_detail_produk = mView.findViewById(R.id.id_detail_produk);

        }
    }


    @Override
    public FragmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.fragment_list, parent, false);
        return new FragmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FragmentViewHolder holder, int position) {
        holder.id_name.setText(DataModel.get(position).getName());
        holder.id_price.setText("Rp. " + DataModel.get(position).getPrice());
        String rating = DataModel.get(position).getRating() == null ? "-" : DataModel.get(position).getRating();

        holder.id_rating.setText("Rating : " + rating);

        final String msg_toast = DataModel.get(position).getId().toString() + " : " + DataModel.get(position).getBrand() + " - " + DataModel.get(position).getName();

        holder.id_click_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, msg_toast, Toast.LENGTH_SHORT).show();

            }
        });

        final String url_produk = DataModel.get(position).getProduct_link();

        holder.id_visit_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebviewActivity.class);
                intent.putExtra("url", url_produk);
                context.startActivity(intent);
            }
        });

        final String url_detail = DataModel.get(position).getProduct_api_url();
        holder.id_detail_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, url_detail, Toast.LENGTH_SHORT).show();
            }
        });

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(DataModel.get(position).getImage_link())
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(holder.coverImage);

    }

    @Override
    public int getItemCount() {
        return DataModel.size();
    }
}
