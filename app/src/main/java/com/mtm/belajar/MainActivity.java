package com.mtm.belajar;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.mtm.belajar.model.DataModel;
import com.mtm.belajar.service.ApiClient;
import com.mtm.belajar.service.GetService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private FragmentAdapter adapter;
    private RecyclerView recyclerView;
    ProgressDialog progressDialog;
    GetService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        try {
            /**Buat Handler Retrofit*/
            service = ApiClient.getRetrofitInstance().create(GetService.class);
            Call<List<DataModel>> call = service.getListMakeUp();
            call.enqueue(new Callback<List<DataModel>>() {
                @Override
                public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {
                    progressDialog.dismiss();
//                    Toast.makeText(MainActivity.this, response.body().get(0).getName(), Toast.LENGTH_SHORT).show();
                    generateDataList(response.body());
                }

                @Override
                public void onFailure(Call<List<DataModel>> call, Throwable throwable) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Gagal Memuat", Toast.LENGTH_SHORT).show();
                }
            });


        } catch (Error ex) {
            Toast.makeText(MainActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();

        }


    }

    /**
     * generate data list method()
     */
    private void generateDataList(List<DataModel> DataModel) {
        recyclerView = findViewById(R.id.customRecyclerView);
        adapter = new FragmentAdapter(this, DataModel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
